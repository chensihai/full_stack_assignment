import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';


export const getPeople = createAsyncThunk(
    'api/people',
    async () => {
        const response = await fetch('http://localhost:8088/api/people/');
        const formattedResponse = await response.json();

        const result = JSON.parse(formattedResponse.people).map((row) => {
            return row.fields;
        })

        return result;
    }
);

export const mapSlice = createSlice({
    name: 'map',
    initialState: {
        people: [],
        isLoading: false,
    },
    extraReducers: {
        [getPeople.pending]: (state) => {
            state.isloading = true;
        },

        [getPeople.fulfilled]: (state, action) => {
            state.people = action.payload;
            state.isloading = false;
        },
        [getPeople.Reject]: (state) => {
            state.isloading = false;
        }
    }
});

export default mapSlice.reducer;

