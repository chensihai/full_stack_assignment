
import React from 'react'

function MyComponent() {

    return (
        <div>
            <h1>Welcome</h1>
        </div>
    )
}

export default React.memo(MyComponent)