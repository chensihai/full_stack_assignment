import React from 'react'
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getPeople } from './mapState';


const containerStyle = {
  width: '1200px',
  height: '800px'
};

const center = {
  lat: 23.8103,
  lng: 90.4125
};

function MyComponent() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: process.env.GMAP_API_KEY
  })

  const [map, setMap] = React.useState(null);
  
  const onLoad = React.useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds(center);
    map.fitBounds(bounds);
    setMap(map);
  }, [])

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null)
  }, [])

  const dispatch = useDispatch();
  const people = useSelector( state=>state.map.people);
  useEffect(()=>{
    dispatch(getPeople())
  }, [dispatch]); 
  

  return isLoaded ? (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={10}
      onLoad={onLoad}
      onUnmount={onUnmount}
    >
       {people.map(person => (
       <Marker
       position={{ lat:  parseFloat(person['lattitude']), lng:parseFloat(person['longtitude']) }} title={person['name']} label={{ text: person['name'], color: '#000', fontSize: '16px', fontWeight: 'bold' }}
       />
      ))}

      {/* <Marker position={{lat:23.8105, lng:90.4126}} title='Simon'  label = {{text: 'Simon',  color: "#000", fontSize: "16px", fontWeight: "bold" }}/>
        <Marker position={{lat:23.8107, lng:90.4123}} title='Jenny'/>
        <Marker position={{lat:23.8102, lng:90.4124}} title='Josh'/>
        <Marker position={{lat:23.8106, lng:90.4122}} title='Kiki'/>
        <Marker position={{lat:23.8103, lng:90.4128}} title='Snowy'/> */}
    </GoogleMap>
  ) : <></>
}

export default React.memo(MyComponent)
// export default MyComponent