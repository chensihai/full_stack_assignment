import './App.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import MAP from "./components/Map";
import HOME from "./components/Home";
import { Provider } from 'react-redux'
import { configureStore } from '@reduxjs/toolkit'
import mapReducer from './components/mapState';
export const store = configureStore({
  reducer: {
    map: mapReducer
  }
})

function App() {

  // console.log(people)
  return (
    <main className="content">
      <h1 className="text-success text-uppercase text-center my-4">
        Full Stack Assignment
      </h1>
      <div className="row">
        <div className="col-md-6 col-sm-10 mx-auto p-0">
          <div className="card p-3">

            <div className="my-5 tab-list">
              <Tabs>
                <TabList>
                  <Tab>Home</Tab>
                  <Tab>MAP</Tab>
                </TabList>

                <TabPanel>
                <Provider store={store} >
                    <div className="App">
                      <HOME />
                    </div>
                  </Provider>
                </TabPanel>
                <TabPanel>
                  <Provider store={store} >
                    <div className="App">
                      <MAP />
                    </div>
                  </Provider>
                </TabPanel>
              </Tabs>
            </div>
            <ul className="list-group list-group-flush">
            </ul>
          </div>
        </div>
      </div>

    </main>
  );
}

export default App;
